

# compiler options
CC = g++
CFLAGS = -Wall

BINDIR = ./bin/
INCLUDEDIR = ./include/

OBJ	= $(BINDIR)main.o $(BINDIR)graph.o $(BINDIR)queue.o $(BINDIR)priority_queue.o

$(BINDIR)output: $(OBJ)
	$(CC) $(CFLAGS) $? -o $(BINDIR)output

$(BINDIR)main.o: main.cpp
	$(CC) -c $< -o $@ 

$(BINDIR)graph.o: graph.cpp ./include/graph.h
	$(CC) -c $< -o $@

$(BINDIR)queue.o: queue.cpp ./include/queue.h 
	$(CC) -c $< -o $@

$(BINDIR)priority_queue.o: priority_queue.cpp ./include/priority_queue.h 
	$(CC) -c $< -o $@

.PHONY: clean cleanall

clean:
	rm -rf $(BINDIR)*.o output
cleanall:
	rm -rf $(BINDIR)*.o