#include "./include/graph_struct.h"
#include "./include/queue_struct.h"
#include "./include/priority_queue_struct.h"
#include "./include/graph.h"
#include "./include/queue.h"
#include "./include/priority_queue.h"

#include <fstream>
#include <iostream>
#include <float.h>

graph createGraph(int n){
	graph g;
	g.dim = n;
	g.nodes = new adjList[n];
	for (int i = 0; i < n; i++)
	    g.nodes[i] = NULL;
	return g;
}


void addArc(graph& g, int u, int v, float w) {
    #ifdef DEBUG
        std::cout << "adding arc between " << u << " and " << v  << " of weight " << w << "\n";
    #endif
    adjNode* temp = new adjNode;
    temp->id = v - 1;
    temp->weight = w;
    temp->next = g.nodes[u - 1];
    g.nodes[u - 1] = temp;
}

void addEdge(graph& g, int u, int v, float w) {
    addArc(g,u,v,w);
    addArc(g,v,u,w);
}


int getDim(graph g){ return g.dim; }
adjList getAdjList(graph g, int u){ return g.nodes[u - 1]; }
int getId(adjNode* l){ return (l->id + 1); }
adjList getNext(adjList l){ return l->next; }
float getAdjWeight(adjNode* l){ return (l->weight); }

graph gBuild(std::ifstream& file, bool o, bool w){
    int dim, u, v, weight;
    
    file >> dim;
    graph g = createGraph(dim);

    while(file >> u >> v){
        if (w){
            file >> weight;
            if (o) addArc(g, u, v, weight);
            else addEdge(g, u, v, weight); 
        }
        else{
            if (o) addArc(g, u, v, 0);
            else addEdge(g, u, v, 0);
        }
    }
    return g;
}

void printGraph(graph g){
    for (int i = 1; i <= g.dim; i++){
        if (getAdjList(g, i) != NULL){
            adjList temp = getAdjList(g, i);
            std::cout << i;
            while(temp){ 
                std::cout  << " --> " << getId(temp);
                temp = getNext(temp);
            }
            std::cout << "\n";
        }
    }
}

void BFS(graph g, int n){
    bool* reached = new bool[getDim(g)];
    for (int i = 0; i < getDim(g); i++)
        reached[i] = false;
    queue q = newQueue();
    q = enqueue(q, n);
    reached[n - 1] = true;
    while(!isEmpty(q)){
        int temp = dequeue(q);
        // visit
        for(adjNode* n = getAdjList(g, temp); n != NULL; n = getNext(n)){
            int nId = getId(n);
            if (!reached[nId - 1]){
                q = enqueue(q, nId);
                reached[nId - 1] = true;
            }
        }
    }
}


bool* connected(graph g, int n){
    bool* reached = new bool[getDim(g)];
    for (int i = 0; i < getDim(g); i++)
        reached[i] = false;
    queue q = newQueue();
    q = enqueue(q, n);
    reached[n - 1] = true;
    while(!isEmpty(q)){
        int temp = dequeue(q);
        // visit
        for(adjNode* n = getAdjList(g, temp); n != NULL; n = getNext(n)){
            int nId = getId(n);
            if (!reached[nId - 1]){
                q = enqueue(q, nId);
                reached[nId - 1] = true;
            }
        }
    }
    return reached;
}

// porcheria, da rifare
void connectedComponent(graph g, bool* reached){
    bool* globalReached = reached;
    std::cout << "connected componentent:";
    for(int  i = 0; i < getDim(g); i++)
        if (reached[i]) 
            std::cout << " " << i + 1;
    std::cout << "\n";
    for (int i = 0; i < getDim(g); i++){
        if (!globalReached[i]){
            bool* reachedI = connected(g, i + 1);
            std::cout << "connected componentent:";
            for(int j = 0; j < getDim(g); j++){
                if(reachedI[j]){
                    std::cout << " " << j + 1;
                }
                globalReached[j] = true;
            }
            std::cout << "\n";
        }
    }
}

void spanningTree(graph g, int n){
    int* spanningTree = new int[getDim(g)];
    bool* reached = new bool[getDim(g)];
    for (int i = 0; i < getDim(g); i++)
        reached[i] = false;
    queue q = newQueue();
    q = enqueue(q, n);
    reached[n - 1] = true;
    while(!isEmpty(q)){
        int temp = dequeue(q);
        //std::cout << temp << " ";
        for(adjNode* n = getAdjList(g, temp); n != NULL; n = getNext(n)){
            int nId = getId(n);
            if (!reached[nId - 1]){
                spanningTree[nId] = temp;
                q = enqueue(q, nId);
                reached[nId - 1] = true;
            }
        }
    }
    for(int i = 0; i < getDim(g); i++)
        if(!reached[i]) 
            return;
    std::cout << "spanning tree:\n";
    for(int i = 1; i <= getDim(g); i++){
        std::cout << "il padre di " << i << " è " << spanningTree[i] << "\n";
    }
}

bool biparted(graph g){
    int n = 1;
    bool* parition = new bool[getDim(g)];
    bool* reached = new bool[getDim(g)];
    for (int i = 0; i < getDim(g); i++)
        reached[i] = false;
    queue q = newQueue();
    q = enqueue(q, n);
    reached[n - 1] = true;
    parition[1] = true;
    while(!isEmpty(q)){
        int temp = dequeue(q);
        for(adjNode* n = getAdjList(g, temp); n != NULL; n = getNext(n)){
            int nId = getId(n);
            if (!reached[nId - 1]){
                parition[nId] = !parition[temp];
                q = enqueue(q, nId);
                reached[nId - 1] = true;
            }
            else{
                if(parition[nId] == parition[temp])
                    return false;
            }
        }
    }
    return true;
}

int* dijkstra(graph g, int s){

    pQueue pQ = NULL;
    float* dist = new float[getDim(g)];
    int* parent = new int[getDim(g)];

    // init
    for(int i = 0; i < getDim(g); i++){
        dist[i] = FLT_MAX;
        parent[i] = s;
        pQ = enqueuePQ(pQ, i, dist[i]);
    }
    dist[s - 1] = 0;

    while(!isEmptyPQ(pQ)){

        int bestNode = dequeuePQ(pQ);

        for(adjNode* node = getAdjList(g, bestNode + 1); node != NULL; node = getNext(node)){

            if (dist[getId(node) - 1] > dist[bestNode] + getAdjWeight(node)){
                dist[getId(node) - 1] = dist[bestNode] + getAdjWeight(node);
                parent[getId(node) - 1] = bestNode + 1;
            }
            pQ = Decrease_Priority(pQ, getId(node) - 1, dist[getId(node) - 1]);
        }
    }
    return parent;
}

int* prim(graph g, int s){
    pQueue pQ = NULL;
    float* dist = new float[getDim(g)];
    int* parent = new int[getDim(g)];

    // init
    for(int i = 0; i < getDim(g); i++){
        dist[i] = FLT_MAX;
        parent[i] = s;
        pQ = enqueuePQ(pQ, i, dist[i]);
    }
    dist[s - 1] = 0;

    while(!isEmptyPQ(pQ)){

        int bestNode = dequeuePQ(pQ);
        
        for(adjNode* node = getAdjList(g, bestNode + 1); node != NULL; node = getNext(node)){

            if (dist[getId(node) - 1] > dist[bestNode]){
                // "relax node"
                dist[getId(node) - 1] = dist[bestNode];
                parent[getId(node) - 1] = bestNode + 1;
            }
            pQ = Decrease_Priority(pQ, getId(node) - 1, dist[getId(node) - 1]);
        }
    }
    return parent;
}