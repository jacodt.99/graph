#ifndef QUEUE_H
#define QUEUE_H

struct qNode;
struct queue;

queue enqueue(queue, int);
int dequeue(queue&);
int first(queue);
bool isEmpty(queue);
queue newQueue();

static qNode* newElem(int);

#endif // QUEUE_H