#ifndef GRAPH_H
#define GRAPH_H

#include <iosfwd>

struct graph;
struct adjNode;
typedef adjNode* adjList;

//----------[ PRIMITIVE ]----------//

//[ CREAZIONE ]
graph createGraph(int);
void addArc(graph&, int, int, float);
void addEdge(graph&, int, int, float);

// [ ACCESSO ]
int getDim(graph);
adjList getAdjList(graph, int);
int getId(adjNode*);
adjNode* getNext(adjList);
float getWeight(adjNode*);


//--------[ ALTRE FUNZIONI ]--------//

graph gBuild(std::ifstream&, bool, bool);
void printGraph(graph);
void BFS(graph, int);
bool* connected(graph, int);
void connectedComponent(graph, bool*);
void spanningTree(graph, int);
bool biparted(graph);
int* dijkstra(graph, int);
int* prim(graph, int);

#endif // GRAPH_H