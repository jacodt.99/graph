#ifndef GRAPH_STRUCT_H
#define GRAPH_STRUCT_H

struct adjNode{
    int id;
    float weight;
    adjNode* next;
};

typedef adjNode* adjList;

struct graph{
    adjList* nodes;
    int dim;
};

#endif // GRAPH_STRUCT_H