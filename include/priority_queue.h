#ifndef PRIORITY_QUEUE_H
#define PRIORITY_QUEUE_H

struct elem;
typedef elem* pQueue;

pQueue enqueuePQ(pQueue, int,float);

/// Metodo dequeue
int dequeuePQ(pQueue&);
int minQueue(pQueue);
pQueue Decrease_Priority(pQueue, int,float);
bool isEmptyPQ(pQueue);

#endif // PRIORITY_QUEUE_H