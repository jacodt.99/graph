#ifndef QUEUE_STRUCT_H
#define QUEUE_STRUCT_H

struct adjNode;

struct qNode {
       int inf;
       qNode* next;
};

typedef qNode* queueList;

struct queue{
	qNode* head;
	qNode* tail;
};

#endif // QUEUE_STRUCT_H