#include "./include/queue_struct.h"
#include "./include/queue.h"

#include <iostream>




queue enqueue(queue q, int inf){
	qNode* temp = new qNode;
	if (q.tail == NULL){
		q.head = q.tail = temp;
		return q;
	}
	q.tail->next = temp;
	q.tail = temp;
}

int dequeue(queue& q){
	if (isEmpty(q)) return 0;
	qNode* tempNode = q.head;
	int tempInf = tempNode->inf;
	q.head = q.head->next;
	delete tempNode;
	return tempInf;
}

int first(queue q){ return q.head->inf; }

bool isEmpty(queue q){
	if (q.head == NULL ) return true;
	else return false;
}

queue newQueue(){
	queue temp;
	temp.head = NULL;
	temp.tail = NULL;
	return temp;
}

static qNode* newElem(int inf){
	qNode* temp = new qNode;
	temp->inf = inf;
	temp->next = NULL;
	return temp;
}
 






























/* codaBFS enqueue(codaBFS c, int i){
	qNode *e=new_elem(i);
	if(c.tail!=NULL)
			c.tail->pun=e;
	c.tail=e;
	if(c.head==NULL)
		c.head=c.tail;
	return c;
}

int dequeue(codaBFS& c){
	int ris=(c.head)->inf;
	c.head=(c.head)->pun;
	return ris;
};

int first(codaBFS c){
	return (c.head)->inf;
};

static qNode* new_elem(int n){
	    qNode* p = new qNode ;
	    p->inf=n;
	    p->pun=NULL;
		return p;

}

bool isEmpty(codaBFS c){
	if(c.head==NULL)
		return true;
	return false;
}

codaBFS newQueue(){
	codaBFS c={NULL, NULL};
	return c;
} */