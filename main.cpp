#include "./include/graph_struct.h"
#include "./include/graph.h"

#include <iostream>
#include <cstring>
#include <fstream>

using namespace std;

void err(const char*, bool);

int main(int argc, char** argv){

    if (argc < 4) err("too few args", 1);

    ifstream file(argv[1], ios_base::in);

    if (!file) err("file open error", 1);

    graph g = gBuild(file, argv[2], argv[3]);
    //graph g = createGraph(7);
    printGraph(g);
    //cout << "\n";
    //connectedComponent(g, connected(g, 1));
    //spanningTree(g, 1);
    //cout << biparted(g);
    int* ms = dijkstra(g, 1);
    cout << "\n";
/*     for(int i = 0; i < getDim(g); i++)
        std::cout << "il padre di " << i + 1 << " è " << ms[i] << "\n"; */
    return 0;
}


void err(const char* desc, bool crit){
    cout << desc;
    if (crit) exit(0);
}